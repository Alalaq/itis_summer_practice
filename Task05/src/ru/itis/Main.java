package ru.itis;

import ru.itis.dto.StudentSignUp;
import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("C:\\Users\\muzik\\Desktop\\itis_summer_practice\\Task05\\resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

       // Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        //System.out.println(student);

        //update test
        Student student = new Student( "Sanya", "Petrov", "ITIS@mail.ru", "qwerty007");
        student.setId(2L);
        studentsRepository.update(student);

        //delete test
        studentsRepository.delete(2L);

        //last method test
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(24));
    }
}

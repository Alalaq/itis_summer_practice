package itis;

import itis.jdbc.SimpleDataSource;
import itis.models.Student;
import itis.repositories.StudentsRepository;
import itis.repositories.StudentsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("C:\\Users\\muzik\\Desktop\\itis_summer_practice\\Task04\\resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        System.out.println(studentsRepository.findAll());

        //save test
        Student student = new Student("Vanya", "Petrov", 42);
        studentsRepository.save(student);

        //findById test
        System.out.println(studentsRepository.findById(13L));
    }
}

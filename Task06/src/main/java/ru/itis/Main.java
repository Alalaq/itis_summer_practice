package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcTemplateImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwerty0")
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);

        //update test
        Student sanya = Student.builder()
                .firstName("Sanya")
                .lastName("Petrov")
                .email("ITIS@mail.ru")
                .password("qwerty007")
                .build();

        sanya.setId(3L);

        studentsRepository.update(sanya);

        //delete test
        studentsRepository.delete(3L);

        //findAllByAgeGreaterThanOrderByIdDesc test
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(24));
    }
}

--1. Найдите номер модели, скорость и размер жесткого диска для всех ПК стоимостью менее 500 дол. Вывести: model, speed и hd
select pc.model as model, pc.speed as speed, pc.hd as hd
 from PC pc
 where pc.price < 500

-- 2. Найдите производителей принтеров. Вывести: maker
select distinct pd.maker from Product pd where pd.type = 'Printer'

-- 3. Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.
 select pc.model, pc.ram, pc.screen
 from Laptop pc
 where pc.price > 1000

-- 4. Найдите все записи таблицы Printer для цветных принтеров.
select * from Printer pr where pr.color = 'y'

-- 5. Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол.
select pc.model as model, pc.speed as speed, pc.hd as hd
 from PC pc
 where pc.price < 600 and (pc.cd = '12x' or pc.cd = '24x')

 -- 6. Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти скорости таких ПК-блокнотов.
 -- Вывод: производитель, скорость.
select distinct pd.maker, pc.speed from Product pd, Laptop pc where pd.model = pc.model and
pc.hd >= 10

-- 7. Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).
select distinct pd.model, pc.price from Product pd inner join PC pc
on pd.model = pc.model and pd.maker = 'B' 
union
select distinct pd.model, pc.price from Product pd inner join Laptop pc
on pd.model = pc.model and pd.maker = 'B' 
union
select distinct pd.model, pc.price from Product pd inner join Printer pc
on pd.model = pc.model and pd.maker = 'B'


-- 8. Найдите производителя, выпускающего ПК, но не ПК-блокноты.
select distinct pd.maker from Product pd where pd.type = 'PC'
except
(select distinct pd.maker from Product pd where pd.type = 'Laptop')


-- 9.
select distinct maker from Product where model in (select distinct model from 
PC where speed >= 450)


-- 10. Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price
select distinct model, price from printer where price in (select max(price) from
printer)

-- 11. Найдите среднюю скорость ПК.
select avg(speed) from PC

-- 12. Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.
select avg(speed) from laptop where price > 1000

-- 13. Найдите среднюю скорость ПК, выпущенных производителем A.
select avg(speed) from pc where model in (select distinct model from product 
where maker = 'A')


-- 14. Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.
select distinct sh.class, sh.name, cl.country from ships sh, classes cl where sh.class = cl.class and cl.numGuns >= 10


-- 15. Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
select distinct hd from pc group by hd having count(hd) >= 2


-- 16. Найдите пары моделей PC, имеющих одинаковые скорость и RAM.
-- В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i), 
--Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM.
select distinct pc.model, opc.model, pc.speed, pc.ram from PC pc, PC opc where pc.speed = opc.speed and
pc.ram = opc.ram and not (pc.model = opc.model) and pc.model > opc.model


-- 17. Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
--Вывести: type, model, speed

select distinct pd.type, pd.model, lp.speed from product pd, laptop lp where lp.speed < all(select speed from pc) 
and pd.type = 'Laptop' and pd.model = lp.model


-- 18. Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price
select distinct pd.maker, pr.price from product pd, printer pr where pr.model = 
pd.model and pr.price = (select min(price) from printer where color = 'y') and pr.color = 'y'


-- 19. Для каждого производителя, имеющего модели в таблице Laptop, найдите средний размер экрана выпускаемых им ПК-блокнотов.
--Вывести: maker, средний размер экрана.
select pd.maker, avg(lp.screen) from product pd, laptop lp where lp.model = pd.model group by maker

-- 20. Найдите производителей, выпускающих по меньшей мере три различных модели ПК. Вывести: Maker, число моделей ПК.
select pd.maker as Maker, count(pd.model) as Count_Model from product pd where pd.type = 'PC' group by
pd.maker having count(pd.model) >= 3

-- 21. Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
--Вывести: maker, максимальная цена.
select maker, max(price) from product inner join pc on product.model = pc.model group by maker

-- 22. Для каждого значения скорости ПК, превышающего 600 МГц, определите среднюю цену ПК с такой же скоростью. Вывести: speed, средняя цена.
select speed, avg(price) from pc where speed > 600 group by speed

-- 23. Найдите производителей, которые производили бы как ПК
--со скоростью не менее 750 МГц, так и ПК-блокноты со скоростью не менее 750 МГц.
--Вывести: Maker

select distinct maker from product pd inner join pc pc on pd.model = pc.model where pc.speed >= 750
intersect
select distinct maker from product pd inner join laptop pc on pd.model = pc.model where pc.speed >= 750

-- 24. Перечислите номера моделей любых типов, имеющих самую высокую цену по всей имеющейся в базе данных продукции.
with Prices as (
select distinct model, price from laptop where laptop.price = (select max(price) from laptop)  
union
select distinct model, price from pc where pc.price = (select max(price) from pc)  
union
select distinct model, price from printer where printer.price = (select max(price) from printer)  
)

select model from 
Prices where price >= (select max(price) from Prices)


-- 25. Найдите производителей принтеров, которые производят ПК с наименьшим объемом RAM и с самым быстрым процессором среди всех ПК,
-- имеющих наименьший объем RAM. Вывести: Maker

select distinct maker from product where type = 'Printer'
intersect 
select distinct product.maker from product inner join pc on pc.model = product.model  
where product.type = 'PC' and pc.ram = (select min(ram) from pc)  
and pc.speed = (select max(speed) from (select distinct speed from pc 
where pc.ram = (select min(ram) from pc)) as speed)

-- 26. Найдите среднюю цену ПК и ПК-блокнотов, выпущенных производителем A (латинская буква). Вывести: одна общая средняя цена.

with Price as (
select price from pc where model in (select model from product
where maker = 'A')
union all
select price from Laptop where model in (select model from product
where maker = 'A')
)

select avg(price) as AVG_price from Price

-- 27. Найдите средний размер диска ПК каждого из тех производителей, которые выпускают и принтеры. Вывести: maker, средний размер HD.
select pd.maker, avg(pc.hd) from pc pc, product pd 
where pd.model = pc.model and pd.maker in (select distinct maker from product where type = 'Printer') group by pd.maker

-- 28. Используя таблицу Product, определить количество производителей, выпускающих по одной модели.
with Makers as (
select maker from product group by maker having count(model) = 1 )

select count(*)  from Makers

-- 29. В предположении, что приход и расход денег на каждом пункте приема фиксируется не чаще одного раза в день
-- [т.е. первичный ключ (пункт, дата)], написать запрос с выходными данными (пункт, дата, приход, расход). Использовать таблицы Income_o и Outcome_o.
with t as (select point, date, inc, null as out from Income_o  
Union all
select point, date, null as inc, Outcome_o.out from Outcome_o)

select t.point, t.date, sum(t.inc), sum(t.out) from t group by t.point, t.date

-- 30. В предположении, что приход и расход денег на каждом пункте приема фиксируется произвольное число раз (первичным ключом в таблицах является столбец code), требуется получить таблицу, в которой каждому пункту за каждую дату выполнения операций будет соответствовать одна строка.
--Вывод: point, date, суммарный расход пункта за день (out), суммарный приход пункта за день (inc). Отсутствующие значения считать неопределенными (NULL).
with t as (select point, date, inc, null as out from Income  
Union all
select point, date, null as inc, Outcome.out from Outcome)

select t.point, t.date, sum(t.out), sum(t.inc) from t group by t.point, t.date

-- 31.  Для классов кораблей, калибр орудий которых не менее 16 дюймов, укажите класс и страну.
select distinct cl.class, cl.country from classes cl where cl.bore >= 16

-- 32. Одной из характеристик корабля является половина куба калибра его главных орудий (mw).
-- С точностью до 2 десятичных знаков определите среднее значение mw для кораблей каждой страны, у которой есть корабли в базе данных.

select distinct country, cast(avg((power(bore,3)/2)) as numeric(6,2)) as weight 
from (select C.class, S.name, C.country, C.bore  from classes as c join ships as s on c.class = s.class 
union 
select C.class, O.ship, C.country, C.bore from classes as c join outcomes as o on c.class=o.ship ) as g
group by country 

-- 33. Укажите корабли, потопленные в сражениях в Северной Атлантике (North Atlantic). Вывод: ship.
select ship from outcomes, battles where result = 'sunk' and battle = 'North Atlantic' group by ship


-- 34. По Вашингтонскому международному договору от начала 1922 г. запрещалось строить линейные корабли водоизмещением более 35 тыс.тонн.
-- Укажите корабли, нарушившие этот договор (учитывать только корабли c известным годом спуска на воду). Вывести названия кораблей.
select distinct name from ships inner join classes 
on ships.class = classes.class and ships.launched >= 1922 and classes.type = 'bb' and classes.displacement > 35000 and ships.launched is not null

-- 35. В таблице Product найти модели, которые состоят только из цифр или только из латинских букв (A-Z, без учета регистра).
--Вывод: номер модели, тип модели.

select distinct model, type from product where model NOT like '%[^a-z]%' or model NOT like '%[^0-9]%'

-- 36. Перечислите названия головных кораблей, имеющихся в базе данных (учесть корабли в Outcomes).
select distinct name from ships inner join outcomes on outcomes.ship != ships.name and ships.name = ships.class
union
select distinct ship from outcomes inner join ships on outcomes.ship != ships.name and outcomes.ship in (select distinct class from classes)

-- 37. Найдите классы, в которые входит только один корабль из базы данных (учесть также корабли в Outcomes).
with t as (
select name, class from ships  
union  
select class as name, class  from classes, outcomes  where classes.class = outcomes.ship)  

Select class from t group by class having count(t.name) = 1

-- 38. Найдите страны, имевшие когда-либо классы обычных боевых кораблей ('bb') и имевшие когда-либо классы крейсеров ('bc').
select distinct country from classes where type = 'bc'
INTERSECT 
select distinct country from classes where type = 'bb'

-- 39. Найдите корабли, `сохранившиеся для будущих сражений`; т.е. выведенные из строя в одной битве (damaged), они участвовали в другой, произошедшей позже.
select distinct ot.ship from outcomes ot inner join battles bt on ot.battle = bt.name where result = 'damaged' and
exists (select distinct date from battles inner join outcomes om on om.battle = battles.name and date > bt.date and om.ship = ot.ship)


-- 40. Найти производителей, которые выпускают более одной модели, при этом все выпускаемые производителем модели являются продуктами одного типа.
--Вывести: maker, type
with A as (select maker, type from Product group by maker, type)

select maker, type from Product 
where maker in (select maker  
from A  
group by maker having count(*) = 1)  
group by maker, type having count(*) > 1 
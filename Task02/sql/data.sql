insert into client(first_name, last_name, age, rating)
VALUES ('Anton', 'Gaveev', 19, 2.3),
       ('Amir', 'Gilmutzyanov', 59, 5.0),
       ('Nastya', 'Pushkina', 20, 4.9),
       ('Damir', 'Shamsutdinov', 32, 4.7);

insert into driver(first_name, last_name, age, rating)
VALUES ('Muhanamentnjon', 'Abdumalinalilov', 49, 4.3),
       ('Abdul Halil', 'Abi Ramil', 55, 5.0),
       ('Rahmat', 'Bigzur', 42, 4.6),
       ('Vodinalil', 'Sharganutrov', 37, 2.1);

insert into car(brand, dateofrelease, iselectrical, driversid)
VALUES ('Audi', '2013-04-01', false, 1),
       ('BMW', '2020-01-01', true, 2),
       ('Honda', '2016-07-08', true, 3),
       ('Lada', '2000-03-09', false, 4);

insert into clientsorder(clientsid, driversid, start_time, finish_time, costofthetrip, clientsevaluation, driversevaluation, commentaries)
VALUES (1, 3, '11:03:22', '11:29:32', 321, 5, 3, 'Pretty good'),
       (2, 4, '21:03:12', '21:10:52', 321, 4, 5, 'As good as expected'),
       (3, 1, '01:03:22', '03:29:32', 512, 5, 1, 'Thanks for helping'),
       (4, 2, '17:59:22', '18:05:32', 160, 5, 5, 'It was very fast and comfortable');


update clientsorder
set driversevaluation = 4
where id = 1;